data "aws_iam_policy_document" "sandbox" {
  statement {
  actions = ["sts:AssumeRole"]
    principals {
        type        = "Service"
        identifiers = ["ec2.amazonaws.com", "ssm.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "sandbox_role" {
  name = "sandbox_role"
  path = "/"

  assume_role_policy = "${data.aws_iam_policy_document.sandbox.json}"
}

resource  "aws_iam_instance_profile" "sandbox_profile" {
  name  = "sandbox_profile"
  role  = "${aws_iam_role.sandbox_role.name}"
}

resource       "aws_iam_role_policy_attachment" "sandbox_attach" {
  role       = "${aws_iam_role.sandbox_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

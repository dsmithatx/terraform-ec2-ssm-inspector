resource "aws_ssm_association" "ssm_agent_update" {
  schedule_expression = "cron(0 17 * * ? *)"
  name         = "AWS-UpdateSSMAgent"
  targets {
    key    = "tag:SSM"
    values = ["update"]
  }
}

#!/bin/bash
sudo apt-get update

# Install AWS Systems State Manager
cd /tmp			
wget https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/debian_amd64/amazon-ssm-agent.deb
sudo dpkg -i amazon-ssm-agent.deb
sudo systemctl enable amazon-ssm-agent

# Install Amazon Inspector
cd /tmp
wget https://d1wk0tztpsntt1.cloudfront.net/linux/latest/install
sudo bash install
sudo systemctl enable awsagent

# Upgrade Ubuntu packages - this will take some time to complete after the instance initializes.
DEBIAN_FRONTEND=noninteractive sudo apt-get -yq upgrade
